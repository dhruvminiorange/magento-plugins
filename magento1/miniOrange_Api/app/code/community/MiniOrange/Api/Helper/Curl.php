<?php

class MiniOrange_Api_Helper_Curl extends Mage_Core_Helper_Abstract
{
    public static function get_customer_key($email, $password)
    {
        $url 		 = MiniOrange_Api_Helper_Data::HOSTNAME. "/moas/rest/customer/key";
        $customerKey = MiniOrange_Api_Helper_Data::DEFAULT_CUSTOMER_KEY;
        $apiKey 	 = MiniOrange_Api_Helper_Data::DEFAULT_API_KEY;
        $fields = array (
            'email' 	=> $email,
            'password'  => $password
        );
        $json = json_encode($fields);
        $authHeader = self::createAuthHeader($customerKey,$apiKey);
        $response = self::callAPI($url, $json, $authHeader);
        return $response;
    }


    public static function forgot_password($email)
    {
        $url 		 = MiniOrange_Api_Helper_Data::HOSTNAME . '/moas/rest/customer/password-reset';
        $customerKey = MiniOrange_Api_Helper_Data::DEFAULT_CUSTOMER_KEY;
        $apiKey 	 = MiniOrange_Api_Helper_Data::DEFAULT_API_KEY;

        $fields 	 = array( 'email' => $email);

        $authHeader  = self::createAuthHeader($customerKey,$apiKey);
        $response    = self::callAPI($url, $fields, $authHeader);
        return $response;
    }


    private static function createAuthHeader($customerKey, $apiKey)
    {
        $currentTimestampInMillis = self::getTimestamp();
        if(MiniOrange_Api_Helper_MoApi::isBlank($currentTimestampInMillis))
        {
            $currentTimestampInMillis = round(microtime(true) * 1000);
            $currentTimestampInMillis = number_format($currentTimestampInMillis, 0, '', '');
        }
        $stringToHash = $customerKey . $currentTimestampInMillis . $apiKey;
        $authHeader = hash("sha512", $stringToHash);

        $header = array (
            "Content-Type: application/json",
            "Customer-Key: $customerKey",
            "Timestamp: $currentTimestampInMillis",
            "Authorization: $authHeader"
        );
        return $header;
    }


    private static function getTimestamp()
    {
        $url = MiniOrange_Api_Helper_Data::HOSTNAME . '/moas/rest/mobile/get-timestamp';
        return self::callAPI($url,null,null);
    }


    private static function callAPI($url, $json_string, $headers = array("Content-Type: application/json"))
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        if(!is_null($headers)) curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        if(!is_null($json_string)) curl_setopt($ch, CURLOPT_POSTFIELDS, $json_string);
        $content = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Request Error:' . curl_error($ch);
            exit();
        }

        curl_close($ch);
        return $content;
    }
}