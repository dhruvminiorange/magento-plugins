<?php

namespace MiniOrange\OAuth\Controller\Actions;

use MiniOrange\OAuth\Helper\Curl;
use MiniOrange\OAuth\Helper\OAuthConstants;
use MiniOrange\OAuth\Helper\OAuthMessages;
use MiniOrange\OAuth\Helper\Exception\OTPSendingFailedException;
use MiniOrange\OAuth\Controller\Actions\BaseAdminAction;

/**
 * Handles processing of the Send OTP to Phone form from the 
 * validation page. This action class just processes the phone
 * number provided by the user as an alternative to validate
 * himself for the registration process.
 */
class SendOTPToPhone extends BaseAdminAction
{
    private $REQUEST;


    /**
	 * Execute function to execute the classes function. 
     * 
	 * @throws \Exception
	 */
	public function execute()
	{
        $this->checkIfRequiredFieldsEmpty(array('phone'=>$this->REQUEST));
		$phone = $this->REQUEST['phone'];
        $this->startVerificationProcess('',$phone);
    }


    /**
     * Function calls the Curl function to send the OTP 
     * to the phone number provided by the admin.
     * 
     * @param $result
     * @param $phone
     */
    private function startVerificationProcess($result,$phone)
    {
        $result = Curl::mo_send_otp_token(OAuthConstants::OTP_TYPE_PHONE,'',$phone);
        $result = json_decode($result,true);
        if(strcasecmp($result['status'], 'SUCCESS') == 0)
            $this->handleOTPSentSuccess($result,$phone);
        else
            $this->handleOTPSendFailed();
    }


    /**
     * This function is called to handle what should happen
     * after OTP has been sent successfully to the user's 
     * phone. Show him the validate OTP screen.
     * Set the Transaction ID and otpType in session so
     * that it can fetched later on.
     * 
     * @param $result
     * @param $phone
     */
    private function handleOTPSentSuccess($result,$phone)
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::TXT_ID,$result['txId']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::CUSTOMER_PHONE,$phone); // for resend otp purposes
        $this->oauthUtility->setStoreConfig(OAuthConstants::OTP_TYPE,OAuthConstants::OTP_TYPE_PHONE); 
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_VERIFY_EMAIL);
        $this->messageManager->addSuccessMessage(OAuthMessages::parse('PHONE_OTP_SENT',array('phone'=>$phone)));
    }


     /**
     * This function is called to handle what should happen
     * after sending of OTP fails for a phone number.
     */
    private function handleOTPSendFailed()
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_VERIFY_EMAIL);
        throw new OTPSendingFailedException;
    }


    /** Setter for the request Parameter */
    public function setRequestParam($request)
    {
		$this->REQUEST = $request;
		return $this;
    }
}