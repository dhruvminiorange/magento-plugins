<?php

namespace MiniOrange\OAuth\Controller\Actions;

use MiniOrange\OAuth\Helper\Curl;
use MiniOrange\OAuth\Helper\OAuthConstants;
use MiniOrange\OAuth\Helper\OAuthMessages;
use MiniOrange\OAuth\Helper\Exception\OTPSendingFailedException;
use MiniOrange\OAuth\Controller\Actions\BaseAdminAction;

/**
 * Handles processing of the Resend OTP to Phone/Email form from the 
 * validation page. This action checks the session of what type of 
 * validation is the user trying to do and resend the otp to
 * that authentication type.
 */
class ResendOTPAction extends BaseAdminAction
{
    private $REQUEST;

    /**
	 * Execute function to execute the classes function. 
     * 
	 * @throws \Exception
	 */
	public function execute()
	{
        // fetch the last place otp was sent to - phone or email
        $otpType = $this->oauthUtility->getStoreConfig(OAuthConstants::OTP_TYPE); 
        $email = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_EMAIL);
        $phone = $this->oauthUtility->getStoreConfig(OAuthConstants::CUSTOMER_PHONE);
        $this->startVerificationProcess($phone,$email,$otpType);
    }



    /**
     * Function calls the Curl function to send the OTP 
     * to the phone number or email address provided by the admin.
     * 
     * @param $phone
     * @param $email
     * @param $otpType
     */
    private function startVerificationProcess($phone,$email,$otpType)
    {
        $result = Curl::mo_send_otp_token($otpType,$email,$phone);
        $result = json_decode($result,true);
        if(strcasecmp($result['status'], 'SUCCESS') == 0)
            $this->handleOTPSentSuccess($result,$phone,$email,$otpType);
        else
            $this->handleOTPSendFailed();
    }


    /**
     * This function is called to handle what should happen
     * after OTP has been sent successfully to the user's 
     * phone or email. Show him the validate OTP screen.
     * Set the Transaction ID and otpType in session so
     * that it can fetched later on.
     * 
     * @param $result
     * @param $phone
     * @param $email
     * @param $otpType
     */
    private function handleOTPSentSuccess($result,$phone,$email,$otpType)
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::TXT_ID,$result['txId']);
        $this->oauthUtility->setStoreConfig(OAuthConstants::OTP_TYPE,$otpType);
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_VERIFY_EMAIL);
        $message = $otpType==OAuthConstants::OTP_TYPE_PHONE ? OAuthMessages::parse('PHONE_OTP_SENT',array('phone'=>$phone))
                                                         : OAuthMessages::parse('EMAIL_OTP_SENT',array('email'=>$email));
        $this->messageManager->addSuccessMessage($message);
    }


     /**
     * This function is called to handle what should happen
     * after sending of OTP fails for a phone number or email.
     */
    private function handleOTPSendFailed()
    {
        $this->oauthUtility->setStoreConfig(OAuthConstants::REG_STATUS,OAuthConstants::STATUS_VERIFY_EMAIL);
        throw new OTPSendingFailedException;
    }


    /** Setter for the request Parameter */
    public function setRequestParam($request)
    {
		$this->REQUEST = $request;
		return $this;
    }
}